package com.google.errorprone.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE_USE;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ TYPE_USE })
public @interface Immutable {
    // marker annotation with no members
}
