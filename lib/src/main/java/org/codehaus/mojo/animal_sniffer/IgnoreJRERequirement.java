package org.codehaus.mojo.animal_sniffer;

import java.lang.annotation.*;
import static java.lang.annotation.ElementType.TYPE_USE;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({TYPE_USE})
public @interface IgnoreJRERequirement {
}
