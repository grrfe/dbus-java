package org.slf4j;

import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.messages.Message;

public class Logger {
    public void debug(String s, DBusException ex) {
//        System.out.printf("Debug: %s: %s\n", s, ex.getMessage());
    }

    public void debug(String str, Object... args) {
        log("Debug", str, args);
    }

    public void trace(String str, Object... args) {
        log("Trace", str, args);
    }

    public void info(String str, Object... args) {
        log("Info", str, args);
    }

    public void warn(String str, Object... args) {
        log("Warn", str, args);
    }

    public void error(String str, Object... args) {
        log("Error", str, args);
    }

    public void log(String prefix, String str, Object... args) {
//        String msg = String.format(str.replace("{}", "%s"), args);
//        System.out.printf("[%s] %s\n", prefix, msg);
    }

    public boolean isDebugEnabled() {
        return false;
    }

    public boolean isTraceEnabled() {
        return false;
    }


}
