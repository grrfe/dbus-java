plugins {
    java
    kotlin("jvm")
    id("net.nemerosa.versioning") version "3.0.0"
    `maven-publish`
}

group = "org.freedesktop.dbus"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

kotlin {
    jvmToolchain(21)
}

dependencies {

}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
